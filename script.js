looker.plugins.visualizations.add({
  id: "value-multi-comparison",
  label: "[DEV] Value Multi Comparison",
  options: {},

  create: function(element, config) {
    element.innerHTML = `
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
      <style>
        .value-multi-comparison {
          /* Vertical centering */
          height: 100%;
          display: flex;
          flex-direction: column;
          justify-content: left;
          text-align: left;
          font-family: roboto;
        }

        .value-table {
          font-size: 30px;
          width: 25%;
          margin: auto;
          display: flex;
          flex-direction: column;
        }

        .sub-value-cell{
          font-size: 18px;
          padding-bottom: 10px;
        }

        .sub-value-label{
          display: inline;
          color: grey;
          font-size: 14px;
        }

        .main-value-cell{
          padding-bottom: 10px;
          font-size: 30px;
        }

        .main-value-label-cell{
          color: grey;
          padding: 0px;
          margin: 0px;
          font-size: 18px;
        }
      </style>
    `;
    var container = element.appendChild(document.createElement("div"));
    container.className = "value-multi-comparison";
    this._textElement = container.appendChild(document.createElement("div"));

  },

  updateAsync: function(data, element, config, queryResponse, details, done) {
    this.clearErrors();

    if (queryResponse.fields.measures.length <= 1) {
      this.addError({title: "No Measures", message: "This chart requires at least 2 measures"});
      return;
    }

    if (data.length != 1){
      this.addError({title: "Too many rows", message: "This chart can receive only one row"});
      return;
    }

    // Pick the values
    let first_row = data[0];
    let main_value_name = queryResponse.fields.measures[0].label_short;
    let main_value = first_row[queryResponse.fields.measures[0].name];
    let main_value_to_display = main_value.value;
    if (main_value.rendered !== undefined) {
      main_value_to_display = main_value.rendered;
    }

    // let high_sign = '<i class="bi-arrow-up-square-fill" style="color:green"></i>';
    // let low_sign = '<i class="bi-arrow-down-square-fill" style="color:red"></i>';
    let high_sign = '<i class="bi-chevron-double-up" style="color:green"></i>';
    let low_sign = '<i class="bi-chevron-double-down" style="color:red"></i>';

    // Define visualization
    let html_string = `<div class="value-table"><table cellpadding="0" cellspacing="0" border="0">
                            <tr><td class="main-value-label-cell">${main_value_name}</td></tr>
                            <tr><td class="main-value-cell">${main_value_to_display}</td></tr>`;
                            

    // Get sub values and calc percentages
    for(let m = 1; m<queryResponse.fields.measures.length; m++){
      let sub_value = first_row[queryResponse.fields.measures[m].name].value;
      let sub_value_name = queryResponse.fields.measures[m].label_short;
      let percentage = sub_value/main_value.value*100.0;
      let sign = null;
      if(percentage > 1){sign = high_sign;} else {sign=low_sign;}; 
      html_string += `<tr><td class="sub-value-cell">${sign} ${percentage.toFixed(0)}% <p class="sub-value-label">${sub_value_name}</p></td></tr>`;
    }

    html_string += `</table></div>`;
    this._textElement.innerHTML = html_string;

    done()
  }
});
